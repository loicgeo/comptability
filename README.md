# **Objectif de l'application**


L’outil développé à pour objectif d’aider le trésorier d’un comité d’entreprise à la gestion comptable de ses comptes.
Sous une licence `open-source`, les sources sont disponibles sous <i class="icon-provider-github"></i> [`GitHub`][1] et les documents annexes sous <i class="icon-provider-gdrive"></i> `Google Drive`.
________

________

## Un brin de fonctionnel pour commencer

Dans notre cas pratique, le trésorier gère **2 comptes distincts** :

- un **compte de fonctionnement**, où sont historisés tous les mouvements liés aux dépenses de fonctionnement du CE:
 - frais d’avocats,
 - formations des membres du CE,
 - outils et frais liés au travail du CE
   et de la DUP,
 - objets visant à la promotion du CE,
 - etc...
- un **compte social**, où sont enregistrés tous les mouvements liés aux dépenses d’activités proposées par le CE:
 - pratiques sportives en groupe à l’initiative du comité,
 - remboursement partiel de tickets d’entrées à tous lieux culturels comme les musées ou visites de châteaux,
 - sorties familiales,
 - soirées thématiques.

## Pourquoi une gestion séparée des comptes?
Cette gestion séparée des comptes permet au comité d’entreprise de distinguer les frais selon leur origine.
En effet, le budget de [<i class="icon-share"></i>fonctionnement](#un-brin-de-fonctionnel-pour-commencer) est beaucoup plus important que le [<i class="icon-share"></i>social](#un-brin-de-fonctionnel-pour-commencer), d’où l’utilité d’imputer un maximum de frais sur le budget de fonctionnement afin de préserver au mieux la capacité du CE à organiser les activités sociales.

Enfin une vue synthétique des mouvements, listés de manières chronologique et par activité, permet au trésorier, mais aussi à tout autre membre du CE de suivre les différentes activités et leur financement réel.

_________
# **Conception**

## Cas d'utilisation principaux (`trésorier`, `membre du CE`)

### Les acteurs
**<i class="icon-male"></i> `Trésorier`**
<pre>
Le trésorier est un compte administrateur ayant accès à toute l'application. Il n'a aucune restriction et est capable d'attribuer des comptes aux membres du CE.
Il gère également les droits d'envois des mails aux différents membres selon la catégories des message envoyés.
</pre>

**<i class="icon-male"></i> `Membre du CE`**
<pre> Un membre du CE peut visualiser les comptes et mouvements sur les comptes, ainsi que les activités déjà créées.
En revanche il n'a aucun droit d'action sur l'application.
</pre>

![MainUseCases][2]

## Modèle Conceptuel de Données

### Schéma "comptabilityDB"



# Annexes

## Outils utilisés

### <i class="icon-edit"></i> Modélisation
StartUML - http://staruml.sourceforge.net/en/
DBDesigner - http://www.fabforce.net/dbdesigner4/

### <i class="icon-download "></i> Gestionnaire de sources & Wiki
GitHub - <i class="icon-provider-github"></i> https://github.com/

### <i class="icon-cloud "></i> Gestionnaire de documents
GitHub - <i class="icon-provider-gdrive"></i> https://drive.google.com


<!-- Marks -->
  [1]: https://github.com/loicgeo/comptability
  [2]: https://lh4.googleusercontent.com/CC7VTZ2Og7Dw76JjilKIO79Md9QV2mnWi75CTIs2mQgkuH2id07PEW7Mgezd1z-ZW_0unsEf-d0
  [3]: http://math.stackexchange.com/
  [4]: http://daringfireball.net/projects/markdown/syntax "Markdown"
  [5]: https://github.com/jmcmanus/pagedown-extra "Pagedown Extra"
  [6]: http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference
  [7]: https://code.google.com/p/google-code-prettify/
  [8]: http://highlightjs.org/

____
**Extras for features**
> **NOTE:** You can find more information:
>
> - about **Markdown** syntax [here][4],
> - about **Markdown Extra** extension [here][5],
> - about **LaTeX** mathematical expressions [here][6],
> - about **Prettify** syntax highlighting [here][7],
> - about **Highlight.js** syntax highlighting [here][8].

  [^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.

  [^gfm]: **GitHub Flavored Markdown** (GFM) is supported by StackEdit.
